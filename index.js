const { exec } = require("child_process")
const fs = require("fs")
const fetch = require("node-fetch")
const WebSocketConnection = require("ws")
const puppeteer = require("puppeteer-core");

(async () => {
	const chrome = exec("chromium-browser --remote-debugging-port=3222")
	const cdpCapture = []
	const t0 = Date.now()
	let enableCap = false
	const pushCap = (sender, msg) => {
		if (enableCap)
			cdpCapture.push({
				sender,
				timestamp: (Date.now() - t0) / 1000,
				msg: JSON.parse(msg)
			})
	}

	const sleep = timeout => new Promise(r => setTimeout(r, timeout))

	await sleep(2000)
	const wsUrl = (await (await fetch("http://127.0.0.1:3222/json/version")).json()).webSocketDebuggerUrl

	console.log(wsUrl)
	const ws = new WebSocketConnection(wsUrl)
	const transport = {
		send(msg) {
			// console.log("[CDP] " + msg + "\n")
			pushCap("puppeteer", msg)
			ws.send(msg)
		},

		handleMsg(msg) {
			pushCap("chrome", msg)
			if (this.onmessage)
				this.onmessage.call(null, msg)
		},

		close() { }
	}

	ws.addEventListener("open", async () => {
		const browser = await puppeteer.connect({
			transport
		})
		const page = await browser.newPage()
		await page.goto("https://duck.com", {
			waitUntil: "networkidle2"
		})
		await sleep(2000)
		enableCap = true
		await page.mouse.move(100.0, 100.0).then(() => {
			return page.mouse.down()
		})
		await page.mouse.up()
		await page.type("#search_form_input_homepage", "text", { delay: 100 })
		enableCap = false
		await browser.close()
		ws.close()
	})

	ws.addEventListener("message", evt => transport.handleMsg(evt.data))
	ws.addEventListener("close", () => {
		// chrome.kill("SIGKILL")
		fs.writeFileSync("cdpcapture.json", JSON.stringify(cdpCapture))
	})
})()
